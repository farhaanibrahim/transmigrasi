-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2018 at 07:27 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_transmigrasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_id_belu`
--

CREATE TABLE `t_id_belu` (
  `id` int(15) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `no` varchar(100) NOT NULL,
  `potensi` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `luas_apl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_id_belu`
--

INSERT INTO `t_id_belu` (`id`, `ket`, `no`, `potensi`, `kecamatan`, `desa`, `luas_apl`) VALUES
(1, 'Potensi Lahan APL ', ' ', ' ', ' ', ' ', ' '),
(2, ' ', '1', 'RDA-01', 'NANAET DUBESI', 'FOHOEKA', '12.399895439599'),
(3, ' ', ' ', ' ', ' ', 'NANAENOE', '42.9894376415'),
(4, ' ', ' ', ' ', 'Total', ' ', '55.389333081099'),
(5, ' ', ' ', ' ', ' ', ' ', ' '),
(6, ' ', '2', 'RDA-02', 'KAKULUK MESAK', 'DUALAUS', '359.1263463'),
(7, ' ', ' ', ' ', ' ', 'JENILU', '86.8454615401'),
(8, ' ', ' ', ' ', ' ', 'KABUNA', '281.539619891'),
(9, ' ', ' ', ' ', ' ', 'KENEBIBI', '444.264819771'),
(10, ' ', ' ', ' ', ' ', 'LEOSAMA', '353.709433901'),
(11, ' ', ' ', ' ', 'TASIFETO TIMUR', 'TULAKADI', '98.235654196'),
(12, ' ', ' ', ' ', 'Total', ' ', '1623.7213355991'),
(13, ' ', ' ', ' ', ' ', ' ', ' '),
(14, ' ', '2', 'RDA-03', 'LAMAKNEN SELATAN', 'HENES', '156.925356841'),
(15, ' ', ' ', ' ', ' ', 'LAKMARAS', '188.637580204'),
(16, ' ', ' ', ' ', ' ', 'LOONUNA', '169.814404189'),
(17, ' ', ' ', ' ', ' ', 'LUTHARATO', '608.375244047'),
(18, ' ', ' ', ' ', 'Total ', ' ', '1123.752585281'),
(19, ' ', ' ', ' ', ' ', ' ', ' '),
(20, ' ', ' ', 'RDA-04', 'RAIMANUK', 'TASAIN', '67.3650632181'),
(21, ' ', ' ', ' ', ' ', 'TEUN', '217.406027022'),
(22, ' ', ' ', ' ', 'Total ', ' ', '284.7710902401'),
(23, ' ', ' ', ' ', ' ', ' ', ' '),
(24, ' ', ' ', 'RDA-05', 'TASIFETO BARAT', 'DEROKFATURENE', '426.185921448'),
(25, ' ', ' ', ' ', ' ', 'LOOKEU', '56.0435311102'),
(26, ' ', ' ', ' ', ' ', 'NAEKASA', '1499.457801036'),
(27, ' ', ' ', ' ', 'TASIFETO TIMUR', 'DAFALA', '436.054342831'),
(28, ' ', ' ', ' ', ' ', 'FATUBAA', '1522.76646554'),
(29, ' ', ' ', ' ', ' ', 'MANLETEN', '819.575677376'),
(30, ' ', ' ', ' ', ' ', 'TIALAI', '318.847393589'),
(31, ' ', ' ', ' ', 'Total', ' ', '5078.9311329302'),
(32, ' ', ' ', ' ', ' ', ' ', ' '),
(33, ' ', ' ', 'RDA-06', 'TASIFETO BARAT', 'BAKUSTULAMA', '450.790932074'),
(34, ' ', ' ', ' ', ' ', 'NAEKASA', '1499.457801036'),
(35, ' ', ' ', ' ', 'Total', ' ', '1950.24873311'),
(36, ' ', ' ', ' ', ' ', ' ', ' '),
(37, ' ', ' ', 'RDA-07', 'TASIFETO BARAT', 'BAKUSTULAMA', '1803.163728296'),
(38, ' ', ' ', ' ', ' ', 'DEROKFATURENE', '142.061973816'),
(39, ' ', ' ', ' ', ' ', 'LAWALUTOLUS', '11.9705241581'),
(40, ' ', ' ', ' ', ' ', 'NAITIMU', '378.628735957'),
(41, ' ', ' ', ' ', 'Total', ' ', '2335.8249622271'),
(42, ' ', ' ', ' ', ' ', ' ', ' '),
(43, ' ', ' ', ' ', 'TOTAL POTENSI', ' ', '12452.639172469'),
(44, ' ', ' ', ' ', ' ', ' ', ' '),
(45, 'Komoditas Unggulan', ' ', 'Pertanian Tanaman Pangan', 'Buah-Buahan', 'Perikanan', ' '),
(46, ' ', '1', 'jagung', 'salak', 'tuna', ' '),
(47, ' ', '2', 'ubi kayu', 'nangka', 'cakalang', ' '),
(48, ' ', '3', 'bawang putih', 'Mangga', 'tongkol', ' '),
(49, ' ', '4', 'kubis', 'jeruk', 'tenggiri', ' '),
(50, ' ', '5', 'kacang hijau', 'jambu biji', 'alu-alu', ' '),
(51, ' ', '6', ' ', 'sirsak', 'kakap merah', ' '),
(52, ' ', '7', ' ', 'kangkung', 'kerapu lumpur', ' '),
(53, ' ', '8', ' ', 'tomat', ' ', ' '),
(54, ' ', ' ', ' ', ' ', ' ', ' '),
(55, 'Daya Tampung Kawasan Potensi Transmigrasi', ' ', 'Lahan', ' ', ' ', 'Luas (Ha)'),
(56, ' ', '1', 'SKP', ' ', ' ', ' '),
(57, ' ', '2', 'Lahan untuk PSU', ' ', ' ', ' '),
(58, ' ', '3', 'Lahan Untuk Kawasan Permukiman', ' ', ' ', ' '),
(59, ' ', '4', 'Daya ampung Kawasan (per KK)', ' ', ' ', ' '),
(60, ' ', ' ', 'total lahan potensi transmigrasi', ' ', ' ', ' '),
(61, ' ', ' ', ' ', ' ', ' ', ' '),
(62, 'IndikasiProgram', ' ', 'SP Pugar', ' ', ' ', ' '),
(63, ' ', ' ', ' ', ' ', ' ', ' '),
(64, 'Komoditas Unggulan', ' ', 'Kecamatan', 'Perkebunan', ' ', ' '),
(65, ' ', '1', 'KAKULUK MESAK', 'Kapuk', ' ', ' '),
(66, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(67, ' ', ' ', ' ', 'Jambu Mete', ' ', ' '),
(68, ' ', '2', 'LAMAKNEN SELATAN', 'Kapuk', ' ', ' '),
(69, ' ', ' ', ' ', 'Kemiri', ' ', ' '),
(70, ' ', ' ', ' ', 'Pinang', ' ', ' '),
(71, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(72, ' ', ' ', ' ', 'Kopi', ' ', ' '),
(73, ' ', '3', 'NANAET DUBESI', 'Kemiri', ' ', ' '),
(74, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(75, ' ', '4', 'RAIMANUK', 'Kemiri', ' ', ' '),
(76, ' ', ' ', ' ', 'Pinang', ' ', ' '),
(77, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(78, ' ', '5', 'TASIFETO BARAT', 'Kemiri', ' ', ' '),
(79, ' ', ' ', ' ', 'Jambu Mete', ' ', ' '),
(80, ' ', ' ', ' ', 'Pinang', ' ', ' '),
(81, ' ', '6', 'TASIFETO TIMUR', 'Kapuk', ' ', ' '),
(82, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(83, ' ', ' ', ' ', ' ', ' ', ' '),
(84, 'Pola Usaha', ' ', 'Kecamatan', 'Pola', ' ', ' '),
(85, ' ', '1', 'KAKULUK MESAK', 'Pertanian', ' ', ' '),
(86, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(87, ' ', '2', 'LAMAKNEN SELATAN', 'Pertanian', ' ', ' '),
(88, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(89, ' ', '3', 'NANAET DUBESI', 'Perkebunan', ' ', ' '),
(90, ' ', '4', 'RAIMANUK', 'Perikanan', ' ', ' '),
(91, ' ', '5', 'TASIFETO BARAT', 'Pertanian', ' ', ' '),
(92, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(93, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(94, ' ', '6', 'TASIFETO TIMUR', 'Pertanian', ' ', ' '),
(95, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(96, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(97, ' ', ' ', ' ', ' ', ' ', ' '),
(98, ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `t_id_bengkayang`
--

CREATE TABLE `t_id_bengkayang` (
  `id` int(15) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `no` varchar(100) NOT NULL,
  `potensi` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `luas_apl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_id_bengkayang`
--

INSERT INTO `t_id_bengkayang` (`id`, `ket`, `no`, `potensi`, `kecamatan`, `desa`, `luas_apl`) VALUES
(1, 'Potensi Lahan APL ', ' ', ' ', ' ', ' ', ' '),
(2, ' ', '1', 'POTENSI MONTERADO', 'CAPKALA', 'CAPKALA', '511.513347835'),
(3, ' ', ' ', ' ', 'MONTERADO', 'BERINGIN BARU', '6209.9662253'),
(4, ' ', ' ', ' ', ' ', 'GERANTUNG', '2748.7871965'),
(5, ' ', ' ', ' ', ' ', 'GOA BOMA', '3493.98804362'),
(6, ' ', ' ', ' ', ' ', 'JAHANDUNG', '3186.85086558'),
(7, ' ', ' ', ' ', ' ', 'MEKAR BARU', '2341.99654007'),
(8, ' ', ' ', ' ', ' ', 'MONTERADO', '2373.73392883'),
(9, ' ', ' ', ' ', ' ', 'NEK GINAP', '5800.33856098'),
(10, ' ', ' ', ' ', ' ', 'NYEMPEN', '2323.78711241'),
(11, ' ', ' ', ' ', ' ', 'RANTAU', '1904.83478115'),
(12, ' ', ' ', ' ', ' ', 'SENDORENG', '2581.20312598'),
(13, ' ', ' ', ' ', ' ', 'SERINDU', '2526.89203811'),
(14, ' ', ' ', ' ', 'SAMALANTAN', 'BABANE', '710.708077106'),
(15, ' ', ' ', ' ', ' ', 'BUKIT SERAYAN', '2643.81397281'),
(16, ' ', ' ', ' ', ' ', 'MARUNSU', '3885.2829477'),
(17, ' ', ' ', ' ', ' ', 'PASTIJAYA', '98.2986761893'),
(18, ' ', ' ', ' ', ' ', 'SABAU', '1764.31875131'),
(19, ' ', ' ', ' ', ' ', 'SAMALANTAN', '1549.98695871'),
(20, ' ', ' ', ' ', ' ', 'TUMIANG', '1441.30301206'),
(21, ' ', ' ', ' ', 'total', ' ', '=SUM(G5:G23)'),
(22, ' ', ' ', ' ', ' ', ' ', ' '),
(23, ' ', ' ', 'POTENSI SANGGAU LEDO', 'LEDO', 'DAYUNG', '3626.67096516'),
(24, ' ', ' ', ' ', ' ', 'JESAPE', '3753.88684661'),
(25, ' ', ' ', ' ', ' ', 'LESABELA', '1109.75042784'),
(26, ' ', ' ', ' ', ' ', 'LOMBA KARYA', '3354.07474862'),
(27, ' ', ' ', ' ', ' ', 'RODAYA', '296.864710362'),
(28, ' ', ' ', ' ', ' ', 'SELES', '554.504756568'),
(29, ' ', ' ', ' ', ' ', 'SEMANGAT', '5219.849749924'),
(30, ' ', ' ', ' ', ' ', 'SERANGKAT', '816.379837225'),
(31, ' ', ' ', ' ', 'SANGGAU LEDO', 'BANGE', '2948.64946191'),
(32, ' ', ' ', ' ', ' ', 'DANTI', '7022.57848871'),
(33, ' ', ' ', ' ', ' ', 'GUA', '2742.32893481'),
(34, ' ', ' ', ' ', ' ', 'LEMBANG', '2523.32862702'),
(35, ' ', ' ', ' ', ' ', 'SANGO', '2385.74288054'),
(36, ' ', ' ', ' ', 'SELUAS', 'SAHAN', '894.268710682'),
(37, ' ', ' ', ' ', 'SUTI SEMARANG', 'KELAYU', '1904.31359032'),
(38, ' ', ' ', ' ', ' ', 'SUKA MAJU', '1236.31689387'),
(39, ' ', ' ', ' ', 'TUJUHBELAS', 'KAMUH', '1273.33327971'),
(40, ' ', ' ', ' ', ' ', 'PISAK', '3778.6657885'),
(41, ' ', ' ', ' ', ' ', 'SINAR TEBUDAK', '2548.39069211'),
(42, ' ', ' ', ' ', 'total', ' ', '=SUM(G26:G44)'),
(43, ' ', ' ', ' ', ' ', ' ', ' '),
(44, ' ', ' ', 'SELUAS-JAGOI BABANG', 'JAGOI BABANG', 'JAGOI', '16610.58848544'),
(45, ' ', ' ', ' ', ' ', 'JAGOI SEKIDA', '5578.48220073'),
(46, ' ', ' ', ' ', ' ', 'KUMBA', '19837.6104322'),
(47, ' ', ' ', ' ', ' ', 'SEMUNYING JAYA', '9747.98999662'),
(48, ' ', ' ', ' ', ' ', 'SINAR BARU', '8020.93388173'),
(49, ' ', ' ', ' ', 'SELUAS', 'BENGKAWAN', '7274.33840347'),
(50, ' ', ' ', ' ', ' ', 'KALON', '7299.24159767'),
(51, ' ', ' ', ' ', ' ', 'MAYAK', '6549.17042712'),
(52, ' ', ' ', ' ', ' ', 'SAHAN', '2898.19357705'),
(53, ' ', ' ', ' ', ' ', 'SENTANGAU JAYA', '15052.3818764'),
(54, ' ', ' ', ' ', 'SIDING', 'TANGGUH', '2818.02259602'),
(55, ' ', ' ', ' ', 'SELUAS', 'SELUAS', '4716.59219692'),
(56, ' ', ' ', ' ', 'total', ' ', '=SUM(G47:G58)'),
(57, ' ', ' ', ' ', ' ', ' ', ' '),
(58, ' ', ' ', ' ', 'LUAS TOTAL', ' ', '=SUM(G59,G45,G24)'),
(59, ' ', ' ', ' ', ' ', ' ', ' '),
(60, 'Komoditas Unggulan', ' ', 'Pertanian Tanaman Pangan', 'Buah-Buahan', 'Perikanan', ' '),
(61, ' ', '1', 'Padi', 'alpukat', 'ikan laut', ' '),
(62, ' ', '2', 'Jagung', 'mangga', 'udang', ' '),
(63, ' ', '3', 'Ubi Jalar', 'rambutan', 'bandeng', ' '),
(64, ' ', '4', 'Kacang Tanah', 'belimbing', ' ', ' '),
(65, ' ', '5', 'Kedelai', 'nangka', ' ', ' '),
(66, ' ', '6', 'Kacang Hijau', 'jeruk', ' ', ' '),
(67, ' ', '7', 'Bawang Daun', 'durian', ' ', ' '),
(68, ' ', '8', 'Cabe', 'jambu biji', ' ', ' '),
(69, ' ', '9', 'Kangkung', 'pisang', ' ', ' '),
(70, ' ', '10', 'Bayam', 'petai', ' ', ' '),
(71, ' ', ' ', ' ', ' ', ' ', ' '),
(72, 'Daya Tampung Kawasan Potensi Transmigrasi', ' ', 'Lahan', ' ', ' ', 'Luas (Ha)'),
(73, ' ', '1', 'SKP', ' ', ' ', ' '),
(74, ' ', '2', 'Lahan untuk PSU', ' ', ' ', ' '),
(75, ' ', '3', 'Lahan Untuk Kawasan Permukiman', ' ', ' ', ' '),
(76, ' ', '4', 'Daya ampung Kawasan (per KK)', ' ', ' ', ' '),
(77, ' ', ' ', 'total lahan potensi transmigrasi', ' ', ' ', ' '),
(78, ' ', ' ', ' ', ' ', ' ', ' '),
(79, 'IndikasiProgram', ' ', 'SP Baru', 'SP Pugar', ' ', ' '),
(80, ' ', ' ', ' ', ' ', ' ', ' '),
(81, 'Komoditas Unggulan', ' ', 'Distrik', 'Perkebunan', ' ', ' '),
(82, ' ', '1', 'CAPKALA', 'Karet', ' ', ' '),
(83, ' ', ' ', ' ', 'jarak', ' ', ' '),
(84, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(85, ' ', '2', 'JAGOI BABANG', 'Karet', ' ', ' '),
(86, ' ', ' ', ' ', 'kakao', ' ', ' '),
(87, ' ', '3', 'LEDO', 'Karet', ' ', ' '),
(88, ' ', ' ', ' ', 'Kopi', ' ', ' '),
(89, ' ', '4', 'MONTERADO', 'Karet', ' ', ' '),
(90, ' ', ' ', ' ', 'jarak', ' ', ' '),
(91, ' ', '5', 'SAMALANTAN', 'Karet', ' ', ' '),
(92, ' ', ' ', ' ', 'pinang', ' ', ' '),
(93, ' ', '6', 'SANGGAU LEDO', 'Karet', ' ', ' '),
(94, ' ', ' ', ' ', 'Lada', ' ', ' '),
(95, ' ', ' ', ' ', 'kemiri', ' ', ' '),
(96, ' ', ' ', ' ', 'kakao', ' ', ' '),
(97, ' ', '7', 'SELUAS', 'Karet', ' ', ' '),
(98, ' ', ' ', ' ', 'Lada', ' ', ' '),
(99, ' ', '8', 'SIDING', 'Karet', ' ', ' '),
(100, ' ', ' ', ' ', 'Kelapa Sawit', ' ', ' '),
(101, ' ', ' ', ' ', 'kakao', ' ', ' '),
(102, ' ', '9', 'SUTI SEMARANG', 'Karet', ' ', ' '),
(103, ' ', '10', 'TUJUHBELAS', 'Karet', ' ', ' '),
(104, ' ', ' ', ' ', 'Lada', ' ', ' '),
(105, 'Pola Usaha', ' ', 'Kecamatan', 'Pola', ' ', ' '),
(106, ' ', '1', 'CAPKALA', 'Pertanian', ' ', ' '),
(107, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(108, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(109, ' ', '2', 'JAGOI BABANG', 'Pertanian', ' ', ' '),
(110, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(111, ' ', '3', 'LEDO', 'Pertanian', ' ', ' '),
(112, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(113, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(114, ' ', '4', 'MONTERADO', 'Perkebunan', ' ', ' '),
(115, ' ', '5', 'SAMALANTAN', 'Pertanian', ' ', ' '),
(116, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(117, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(118, ' ', '6', 'SANGGAU LEDO', 'Pertanian', ' ', ' '),
(119, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(120, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(121, ' ', '7', 'SELUAS', 'Pertanian', ' ', ' '),
(122, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(123, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(124, ' ', '8', 'SIDING', 'Pertanian', ' ', ' '),
(125, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(126, ' ', '9', 'SUTI SEMARANG', 'Pertanian', ' ', ' '),
(127, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(128, ' ', '10', 'TUJUHBELAS', 'Perkebunan', ' ', ' '),
(129, ' ', ' ', ' ', ' ', ' ', ' '),
(130, ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `t_id_keerom`
--

CREATE TABLE `t_id_keerom` (
  `id` int(15) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `no` varchar(100) NOT NULL,
  `potensi` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `luas_apl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_id_keerom`
--

INSERT INTO `t_id_keerom` (`id`, `ket`, `no`, `potensi`, `kecamatan`, `desa`, `luas_apl`) VALUES
(1, 'Potensi Lahan APL ', ' ', ' ', ' ', ' ', ' '),
(2, ' ', '1', 'Potensi Kawasan 1', 'Senggi', 'Senggi', '3595.53'),
(3, ' ', ' ', ' ', ' ', 'Warief', '4057.6'),
(4, ' ', ' ', ' ', ' ', ' ', '7653.13'),
(5, ' ', ' ', ' ', 'Web', 'Dubu', '14408.15'),
(6, ' ', ' ', ' ', ' ', 'Yuruf', '691.65'),
(7, ' ', ' ', ' ', ' ', ' ', '15099.8'),
(8, ' ', ' ', ' ', 'Total ', ' ', '22752.93'),
(9, ' ', ' ', ' ', ' ', ' ', ' '),
(10, ' ', '2', 'Potensi Kawasan 2', 'Arso Timur', 'Kibay', '3512.02'),
(11, ' ', ' ', ' ', ' ', 'Kriku', '4307.03'),
(12, ' ', ' ', ' ', ' ', 'Sangke', '4947.35'),
(13, ' ', ' ', ' ', ' ', 'Skofro', '7440.82'),
(14, ' ', ' ', ' ', ' ', 'Suskun', '1666.21'),
(15, ' ', ' ', ' ', ' ', 'Wambes', '514.6'),
(16, ' ', ' ', ' ', ' ', 'Wembi', '3045.28'),
(17, ' ', ' ', ' ', ' ', 'Yetty', '4575.63'),
(18, ' ', ' ', ' ', 'Total', ' ', '30008.94'),
(19, ' ', ' ', ' ', ' ', ' ', ' '),
(20, ' ', ' ', ' ', 'TOTAL LUAS ', ' ', '52761.87'),
(21, ' ', ' ', ' ', ' ', ' ', ' '),
(22, 'Komoditas Unggulan', ' ', 'Pertanian Tanaman Pangan', 'Buah-Buahan', 'Perikanan', ' '),
(23, ' ', '1', 'padi', 'pisang', 'ikan mas', ' '),
(24, ' ', '2', 'jagung', 'nanas', 'ikan nila', ' '),
(25, ' ', '3', 'ubi kayu', 'nangka', 'ikan mujair', ' '),
(26, ' ', '4', 'ubi jalar', 'kedondong', 'ikan lele', ' '),
(27, ' ', '5', 'kacang tanah', 'pepaya', 'belut', ' '),
(28, ' ', '6', 'kacang kedelai', 'jeruk', ' ', ' '),
(29, ' ', '7', 'kacang hijau', 'salak', ' ', ' '),
(30, ' ', '8', 'Keladi', ' ', ' ', ' '),
(31, ' ', ' ', ' ', ' ', ' ', ' '),
(32, 'Daya Tampung Kawasan Potensi Transmigrasi', ' ', 'Lahan', ' ', ' ', 'Luas (Ha)'),
(33, ' ', '1', 'SKP', ' ', ' ', '3742.16'),
(34, ' ', '2', 'Lahan untuk PSU', ' ', ' ', '1137.65'),
(35, ' ', '3', 'Lahan Untuk Kawasan Permukiman', ' ', ' ', '2654.51'),
(36, ' ', '4', 'Daya ampung Kawasan (per KK)', ' ', '1327 kk', ' '),
(37, ' ', ' ', 'total lahan potensi transmigrasi', ' ', ' ', '22752.93'),
(38, ' ', ' ', ' ', ' ', ' ', ' '),
(39, 'IndikasiProgram', ' ', 'SP Pugar', ' ', ' ', ' '),
(40, ' ', ' ', ' ', ' ', ' ', ' '),
(41, 'Komoditas Unggulan', ' ', 'Distrik', 'Perkebunan', ' ', ' '),
(42, ' ', '1', 'Senggi', 'coklat', ' ', ' '),
(43, ' ', ' ', ' ', 'Pinang', ' ', ' '),
(44, ' ', '2', 'Web', 'coklat', ' ', ' '),
(45, ' ', ' ', ' ', 'Pinang', ' ', ' '),
(46, ' ', ' ', ' ', ' ', ' ', ' '),
(47, 'Pola Usaha', ' ', 'Distrik', 'Pola', ' ', ' '),
(48, ' ', '1', 'Senggi', 'Pertanian', ' ', ' '),
(49, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(50, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(51, ' ', '2', 'Web', 'Pertanian', ' ', ' '),
(52, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(53, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(54, ' ', '3', 'Arso Timur', 'Pertanian', ' ', ' '),
(55, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(56, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(57, ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `t_id_sidrap`
--

CREATE TABLE `t_id_sidrap` (
  `id` int(15) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `no` varchar(100) NOT NULL,
  `potensi` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `luas_apl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_id_sidrap`
--

INSERT INTO `t_id_sidrap` (`id`, `ket`, `no`, `potensi`, `kecamatan`, `desa`, `luas_apl`) VALUES
(1, 'Potensi Lahan APL ', ' ', ' ', ' ', ' ', ' '),
(2, ' ', '1', 'Potensi Pitu Riase', 'DUAPITUE', 'BILA', '326.040930589'),
(3, ' ', ' ', ' ', 'total', ' ', '326.040930589'),
(4, ' ', ' ', ' ', ' ', ' ', ' '),
(5, ' ', ' ', ' ', 'PITU RIASE', 'BATU', '5496.552328093'),
(6, ' ', ' ', ' ', ' ', 'BILA RIASE', '3015.7383765242'),
(7, ' ', ' ', ' ', ' ', 'BOLA BULU', '509.165223925'),
(8, ' ', ' ', ' ', ' ', 'BOTTO', '2141.364114117'),
(9, ' ', ' ', ' ', ' ', 'COMPONG', '2129.22352149'),
(10, ' ', ' ', ' ', ' ', 'DENGENG-DENGENG', '76.2877676244'),
(11, ' ', ' ', ' ', ' ', 'LAGADING', '3316.28005306'),
(12, ' ', ' ', ' ', ' ', 'LEPPANGENG', '32.4586736824'),
(13, ' ', ' ', ' ', ' ', 'LOMBO', '2286.22287581'),
(14, ' ', ' ', ' ', 'total', ' ', '=SUM(G8:G16)'),
(15, ' ', ' ', ' ', ' ', ' ', ' '),
(16, ' ', ' ', ' ', 'PITU RIAWA', 'BETAO', '3866.49617262'),
(17, ' ', ' ', ' ', ' ', 'BETAO RIASE', '1023.74026104'),
(18, ' ', ' ', ' ', ' ', 'BULU CENRANA', '6.07272430186'),
(19, ' ', ' ', ' ', ' ', 'KALEMPANG', '864.142370263'),
(20, ' ', ' ', ' ', 'total', ' ', '=SUM(G19:G22)'),
(21, ' ', ' ', ' ', ' ', ' ', ' '),
(22, ' ', ' ', ' ', 'TOTAL LUAS ', ' ', '=SUM(G5:G22)'),
(23, ' ', ' ', ' ', ' ', ' ', ' '),
(24, 'Komoditas Unggulan', ' ', 'Pertanian Tanaman Pangan', 'Buah-Buahan', 'Perikanan', ' '),
(25, ' ', '1', 'padi', 'Durian', 'ikan mas', ' '),
(26, ' ', '2', 'jagung', 'Jambu Biji', 'ikan nila', ' '),
(27, ' ', '3', 'ubi kayu', 'Jambu Air', 'ikan gurame', ' '),
(28, ' ', '4', 'ubi jalar', 'Mangga', 'ikan lele', ' '),
(29, ' ', '5', 'kacang tanah', 'Nangka', 'ikan Patin', ' '),
(30, ' ', '6', 'kacang hijau', 'Pepaya', ' ', ' '),
(31, ' ', '7', 'kedelai', 'Pisang', ' ', ' '),
(32, ' ', '8', 'sayur-mayur', 'Rambutan', ' ', ' '),
(33, ' ', ' ', ' ', ' ', ' ', ' '),
(34, 'Daya Tampung Kawasan Potensi Transmigrasi', ' ', 'Lahan', ' ', ' ', 'Luas (Ha)'),
(35, ' ', '1', 'SKP', ' ', ' ', ' '),
(36, ' ', '2', 'Lahan untuk PSU', ' ', ' ', ' '),
(37, ' ', '3', 'Lahan Untuk Kawasan Permukiman', ' ', ' ', ' '),
(38, ' ', '4', 'Daya ampung Kawasan (per KK)', ' ', ' ', ' '),
(39, ' ', ' ', 'total lahan potensi transmigrasi', ' ', ' ', ' '),
(40, ' ', ' ', ' ', ' ', ' ', ' '),
(41, 'IndikasiProgram', ' ', 'SP Baru', 'SP Pugar', ' ', ' '),
(42, ' ', ' ', ' ', ' ', ' ', ' '),
(43, 'Komoditas Unggulan', ' ', 'Kecamatan', 'Perkebunan', ' ', ' '),
(44, ' ', ' ', 'DUAPITUE', 'Kakao', ' ', ' '),
(45, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(46, ' ', ' ', ' ', 'jambu mete', ' ', ' '),
(47, ' ', ' ', 'PITU RIASE', 'Kakao', ' ', ' '),
(48, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(49, ' ', ' ', ' ', 'cengkeh', ' ', ' '),
(50, ' ', ' ', ' ', 'kopi', ' ', ' '),
(51, ' ', ' ', ' ', 'jambu mete', ' ', ' '),
(52, ' ', ' ', 'PITU RIAWA', 'Kakao', ' ', ' '),
(53, ' ', ' ', ' ', 'Kelapa', ' ', ' '),
(54, ' ', ' ', ' ', 'cengkeh', ' ', ' '),
(55, ' ', ' ', ' ', 'jambu mete', ' ', ' '),
(56, ' ', ' ', ' ', ' ', ' ', ' '),
(57, 'Pola Usaha', ' ', 'Kecamatan', 'Pola', ' ', ' '),
(58, ' ', '1', 'DUAPITUE', 'Pertanian', ' ', ' '),
(59, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(60, ' ', '2', 'PITU RIASE', 'Pertanian', ' ', ' '),
(61, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(62, ' ', '3', 'PITU RIAWA', 'Pertanian', ' ', ' '),
(63, ' ', ' ', ' ', 'Perikanan', ' ', ' '),
(64, ' ', ' ', ' ', 'Perkebunan', ' ', ' '),
(65, ' ', ' ', ' ', ' ', ' ', ' '),
(66, ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `t_skp`
--

CREATE TABLE `t_skp` (
  `id` int(15) NOT NULL,
  `skp` varchar(100) NOT NULL,
  `nm_skp` varchar(100) NOT NULL,
  `luas` varchar(100) NOT NULL,
  `kws_htn_klasifikasi` varchar(100) NOT NULL,
  `kws_htn_luas` varchar(100) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `nm_desa` varchar(100) NOT NULL,
  `status_desa` varchar(100) NOT NULL,
  `pusat_skp_kpb` varchar(100) NOT NULL,
  `potensi_sp` varchar(100) NOT NULL,
  `pola_ush_pokok` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_skp`
--

INSERT INTO `t_skp` (`id`, `skp`, `nm_skp`, `luas`, `kws_htn_klasifikasi`, `kws_htn_luas`, `kecamatan`, `nm_desa`, `status_desa`, `pusat_skp_kpb`, `potensi_sp`, `pola_ush_pokok`) VALUES
(301, 'a', 'SKP A = WOSLAY', '40403.37', 'APL', '13040.91', 'Senggi', 'Desa Molof', 'Berkembang', 'Pusat SKP', 'Tempatan', 'Pertanian; Padi; Jagung; Kacang; Kedelai'),
(302, 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(303, 'a', ' ', ' ', 'HPK', '17612.97', ' ', 'Desa Molof', 'Berkembang', ' ', 'Tempatan', ' '),
(304, 'a', ' ', ' ', ' ', ' ', ' ', 'Desa Molof', 'Berkembang', ' ', 'Tempatan', ' '),
(305, 'a', ' ', ' ', 'HPT', '19.44', ' ', ' ', ' ', ' ', ' ', ' '),
(306, 'a', ' ', ' ', 'TUBUH AIR', '110.57', ' ', 'Desa Molof', 'Berkembang', ' ', 'Tempatan', ' '),
(307, 'a', 'Total', '40403.37', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(308, 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(309, 'a', 'Keterangan :', 'KPB', 'Kawasan Perkotaan Baru', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(310, 'a', ' ', 'SKP', 'Satuan Kawasan Pengembangan', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(311, 'a', ' ', 'SP', 'Satuan Pemukiman', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(312, 'a', ' ', 'APL', 'Area Penggunaan Lain', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(313, 'a', ' ', 'HPT', 'Hutan Produksi Terbatas', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(314, 'a', ' ', 'HPK', 'Hutan Produksi yang dapat Dikonversi', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(315, 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(316, 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(317, 'b', 'SKP B = SENGGI', '73382.89', 'APL', '16900.69', 'Senggi', 'Desa Senggi', 'Berkembang', 'Pusat SKP', 'Tempatan', 'Pertanian; Padi; Jagung; Kacang; Kedelai'),
(318, 'b', ' ', ' ', ' ', ' ', ' ', 'Desa Senggi', 'Berkembang', ' ', 'Tempatan', ' '),
(319, 'b', ' ', ' ', 'HPK', '38751.33', ' ', 'Desa Senggi', 'Berkembang', ' ', 'Tempatan', ' '),
(320, 'b', ' ', ' ', 'TUBUH AIR', '156.88', 'Waris', 'Desa Kalifam', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(321, 'b', ' ', ' ', ' ', ' ', ' ', 'Desa Kalifam', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(322, 'b', 'Total', '73382.89', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(323, 'b', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(324, 'b', 'Keterangan :', 'KPB', 'Kawasan Perkotaan Baru', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(325, 'b', ' ', 'SKP', 'Satuan Kawasan Pengembangan', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(326, 'b', ' ', 'SP', 'Satuan Pemukiman', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(327, 'b', ' ', 'APL', 'Area Penggunaan Lain', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(328, 'b', ' ', 'HPT', 'Hutan Produksi Terbatas', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(329, 'b', ' ', 'HPK', 'Hutan Produksi yang dapat Dikonversi', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(330, 'b', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(331, 'b', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(332, 'c', 'SKP C = JABANDA', '42896.97', 'APL', '1814.59', 'Senggi', 'Desa Jabanda', 'Berkembang', 'Pusat SKP', 'Tempatan', 'Pertanian; Padi; Jagung; Kacang; Kedelai'),
(333, 'c', ' ', ' ', ' ', ' ', ' ', 'Desa Jabanda', 'Berkembang', ' ', 'Tempatan', ' '),
(334, 'c', ' ', ' ', 'HPK', '30742.1', 'Waris', 'Desa Yuwainda', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(335, 'c', ' ', ' ', ' ', ' ', 'Senggi', 'Desa Warlef', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(336, 'c', ' ', ' ', ' ', ' ', ' ', 'Desa Warlef', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(337, 'c', 'Total', '42896.97', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(338, 'c', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(339, 'c', 'Keterangan :', 'KPB', 'Kawasan Perkotaan Baru', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(340, 'c', ' ', 'SKP', 'Satuan Kawasan Pengembangan', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(341, 'c', ' ', 'SP', 'Satuan Pemukiman', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(342, 'c', ' ', 'APL', 'Area Penggunaan Lain', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(343, 'c', ' ', 'HPT', 'Hutan Produksi Terbatas', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(344, 'c', ' ', 'HPK', 'Hutan Produksi yang dapat Dikonversi', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(345, 'c', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(346, 'c', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(347, 'd', 'SKP D = YURUF', '65045.19', 'APL', '663.85', 'Web', 'Desa Yuruf', 'Berkembang', 'Pusat SKP', 'Tempatan', 'Pertanian; Padi; Jagung; Kacang; Kedelai'),
(348, 'd', ' ', ' ', ' ', ' ', ' ', 'Desa Yuruf', 'Berkembang', ' ', 'Tempatan', ' '),
(349, 'd', ' ', ' ', 'HPK', '48608.72', ' ', 'Desa Dubu', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(350, 'd', ' ', ' ', ' ', ' ', ' ', 'Desa Umuaf', 'Sangat Tertinggal', ' ', 'Baru', ' '),
(351, 'd', 'Total', '65045.19', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(352, 'd', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(353, 'd', 'Keterangan :', 'KPB', 'Kawasan Perkotaan Baru', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(354, 'd', ' ', 'SKP', 'Satuan Kawasan Pengembangan', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(355, 'd', ' ', 'SP', 'Satuan Pemukiman', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(356, 'd', ' ', 'APL', 'Area Penggunaan Lain', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(357, 'd', ' ', 'HPT', 'Hutan Produksi Terbatas', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(358, 'd', ' ', 'HPK', 'Hutan Produksi yang dapat Dikonversi', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(359, 'd', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(360, 'd', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(361, 'KPB', 'KPB', '5706.25', 'APL', '4335.61', 'Senggi', 'Desa Woslay', 'Berkembang', 'Pusat KPB', 'Tempatan', 'Pertanian seperti Padi, Jagung, Kacang Kedelai dll'),
(362, 'KPB', ' ', ' ', 'TUBUH AIR', '6.44', ' ', 'Desa Senggi', 'Berkembang', ' ', 'Tempatan', ' '),
(363, 'KPB', 'Total', '5706.25', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(364, 'KPB', ' ', ' ', ' ', ' ', ' ', 'Jumlah Desa ', 'Tertinggal', ' ', ' ', ' '),
(365, 'KPB', ' ', ' ', ' ', ' ', ' ', 'Jumlah Desa ', 'Sangat Tertinggal', ' ', ' ', ' '),
(366, 'KPB', ' ', ' ', ' ', ' ', ' ', 'Jumlah Desa ', 'Berkembang', ' ', ' ', ' '),
(367, 'KPB', ' ', ' ', ' ', ' ', ' ', 'Jumlah Desa ', 'Maju', ' ', ' ', ' '),
(368, 'KPB', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(369, 'KPB', 'Keterangan :', 'KPB', 'Kawasan Perkotaan Baru', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(370, 'KPB', ' ', 'SKP', 'Satuan Kawasan Pengembangan', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(371, 'KPB', ' ', 'SP', 'Satuan Pemukiman', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(372, 'KPB', ' ', 'APL', 'Area Penggunaan Lain', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(373, 'KPB', ' ', 'HPT', 'Hutan Produksi Terbatas', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(374, 'KPB', ' ', 'HPK', 'Hutan Produksi yang dapat Dikonversi', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(375, 'KPB', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
(376, 'KPB', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `t_skp_belu`
--

CREATE TABLE `t_skp_belu` (
  `id` int(15) NOT NULL,
  `skp` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_skp_belu`
--

INSERT INTO `t_skp_belu` (`id`, `skp`, `file`, `tgl`) VALUES
(1, 'A', 'NTT_SKP_A.xls', '2018-09-29'),
(2, 'B', 'NTT_SKP_B.xls', '2018-09-29'),
(3, 'C', 'NTT_SKP_C.xls', '2018-09-29'),
(4, 'D', 'NTT_SKP_D.xls', '2018-09-29'),
(5, 'KPB', 'NTT_SKP_KPB.xls', '2018-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `t_skp_bengkayang`
--

CREATE TABLE `t_skp_bengkayang` (
  `id` int(15) NOT NULL,
  `skp` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_skp_bengkayang`
--

INSERT INTO `t_skp_bengkayang` (`id`, `skp`, `file`, `tgl`) VALUES
(1, 'A', 'SKP_A_BENGKAYANG.xls', '2018-09-29'),
(2, 'B KPB', 'SKP_B_KPB_BENGKAYANG.xls', '2018-09-29'),
(3, 'C', 'SKP_C_BENGKAYANG.xls', '2018-09-29'),
(4, 'D', 'SKP_D_BENGKAYANG.xls', '2018-09-29'),
(5, 'E', 'SKP_E_BENGKAYANG.xls', '2018-09-29'),
(6, 'F', 'SKP_F_BENGKAYANG.xls', '2018-09-29'),
(7, 'G', 'SKP_G_BENGKAYANG.xls', '2018-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `t_skp_sidenreng_rappang`
--

CREATE TABLE `t_skp_sidenreng_rappang` (
  `id` int(15) NOT NULL,
  `skp` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_skp_sidenreng_rappang`
--

INSERT INTO `t_skp_sidenreng_rappang` (`id`, `skp`, `file`, `tgl`) VALUES
(1, 'A', 'sulsel_skp_A_SIDENRAPPANG.xls', '2018-09-30'),
(2, 'B', 'sulsel_skp_B_SIDENRAPPANG.xls', '2018-09-30'),
(3, 'C', 'sulsel_skp_C_SIDENRAPPANG.xls', '2018-09-30'),
(4, 'KPB', 'sulsel_skp_kpb_SIDENRAPPANG.xls', '2018-09-30');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` int(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `username`, `password`) VALUES
(1, 'admin', '0c7540eb7e65b553ec1ba6b20de79608');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_id_belu`
--
ALTER TABLE `t_id_belu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_id_bengkayang`
--
ALTER TABLE `t_id_bengkayang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_id_keerom`
--
ALTER TABLE `t_id_keerom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_id_sidrap`
--
ALTER TABLE `t_id_sidrap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_skp`
--
ALTER TABLE `t_skp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_skp_belu`
--
ALTER TABLE `t_skp_belu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_skp_bengkayang`
--
ALTER TABLE `t_skp_bengkayang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_skp_sidenreng_rappang`
--
ALTER TABLE `t_skp_sidenreng_rappang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_id_belu`
--
ALTER TABLE `t_id_belu`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `t_id_bengkayang`
--
ALTER TABLE `t_id_bengkayang`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `t_id_keerom`
--
ALTER TABLE `t_id_keerom`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `t_id_sidrap`
--
ALTER TABLE `t_id_sidrap`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `t_skp`
--
ALTER TABLE `t_skp`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;

--
-- AUTO_INCREMENT for table `t_skp_belu`
--
ALTER TABLE `t_skp_belu`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_skp_bengkayang`
--
ALTER TABLE `t_skp_bengkayang`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_skp_sidenreng_rappang`
--
ALTER TABLE `t_skp_sidenreng_rappang`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
