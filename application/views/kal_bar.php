<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kalimantan Barat</title>
    <!-- Map Box -->
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css' rel='stylesheet' />

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:50px; bottom:0; width:100%; }
    </style>
    <style>
		#menu {
			background: #fff;
			position: absolute;
			z-index: 1;
			top: 60px;
			right: 10px;
			border-radius: 3px;
			width: auto;
			border: 1px solid rgba(0,0,0,0.4);
			font-family: 'Open Sans', sans-serif;
		}

		#menu a {
			font-size: 13px;
			color: #404040;
			display: block;
			margin: 0;
			padding: 0;
			padding: 10px;
			text-decoration: none;
			border-bottom: 1px solid rgba(0,0,0,0.25);
			text-align: center;
		}

		#menu a:last-child {
			border: none;
		}

		#menu a:hover {
			background-color: #f8f8f8;
			color: #404040;
		}

		#menu a.active {
			background-color: #3887be;
			color: #ffffff;
		}

		#menu a.active:hover {
			background: #3074a4;
		}
	</style>
</head>
<body>
	<?php include 'template/navbar.php'; ?>	
    <nav id="menu"></nav>
    <div id='map'></div>
</body>
</html>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
	var map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
		center: [110, 1.1], // starting position [lng, lat]
		zoom: 8 // starting zoom
	});
	var nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'top-left');

    map.on('load', function() {
        //Kalbar_PENETAPAN_Bengkayang_Jagoi Babang Batas SKP 2016
		map.addLayer({
			'id': 'Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/kal_bar/Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016.json'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //kalbar_potensi_ bengkayang
		map.addLayer({
			'id': 'kalbar_potensi_ bengkayang',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/kal_bar/kalbar_potensi_ bengkayang.json'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //kalbar_potensi_bengkayang_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "kalbar_potensi_bengkayang_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/kal_bar/kalbar_potensi_bengkayang_PN.json'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

        //SKP A
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP A",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP A" }, "geometry": { "type": "Point", "coordinates": [ 109.714189035000132, 1.411062387000087 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP B
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP B",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP B" }, "geometry": { "type": "Point", "coordinates": [ 109.893624342664992, 1.376609243215659 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP C
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP C",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP C" }, "geometry": { "type": "Point", "coordinates": [ 109.818755228465946, 1.30247343918528 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP D
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP D",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP D" }, "geometry": { "type": "Point", "coordinates": [ 109.834917449943831, 1.17605742581496 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP E
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP E",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP E" }, "geometry": { "type": "Point", "coordinates": [ 109.680900986448648, 1.211226391672868 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP F
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP F",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP F" }, "geometry": { "type": "Point", "coordinates": [ 109.736042683255548, 1.249246381477045 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP G
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP G",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "ket": "SKP G" }, "geometry": { "type": "Point", "coordinates": [ 109.970870254140195, 1.210275885023531 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});


    });

    var layer = [
        'Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016',
        'kalbar_potensi_ bengkayang',
        'kalbar_potensi_bengkayang_PN',
		'SKP A',
		'SKP B',
		'SKP C',
		'SKP D',
		'SKP E',
		'SKP F',
		'SKP G'
    ];

	layer.forEach(layer => {
		map.on('click', layer, function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML(layer)
				.addTo(map);
		});
	});

	map.on('click', 'kalbar_potensi_bengkayang_PN', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP A', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP A KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_a_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP B', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP B KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_b_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP C', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP C KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_c_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP D', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP D KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_d_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP E', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP E KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_e_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP F', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP F KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_f_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP G', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP G KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_g_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

    for (var i = 0; i < layer.length; i++) {
		var id = layer[i];

		var link = document.createElement('a');
		link.href = '#';
		link.className = 'active';
		link.textContent = id;

		link.onclick = function (e) {
			var clickedLayer = this.textContent;
			e.preventDefault();
			e.stopPropagation();

			var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

			if (visibility === 'visible') {
				map.setLayoutProperty(clickedLayer, 'visibility', 'none');
				this.className = '';
			} else {
				this.className = 'active';
				map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
			}
		};

		var layers = document.getElementById('menu');
		layers.appendChild(link);
	}
</script>