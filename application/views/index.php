<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kab. Keerom</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Map Box -->
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css' rel='stylesheet' />

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:50px; bottom:0; width:100%; }
    </style>
	<style>
		#menu {
			background: #fff;
			position: absolute;
			z-index: 1;
			top: 60px;
			right: 10px;
			border-radius: 3px;
			width: auto;
			border: 1px solid rgba(0,0,0,0.4);
			font-family: 'Open Sans', sans-serif;
		}

		#menu a {
			font-size: 13px;
			color: #404040;
			display: block;
			margin: 0;
			padding: 0;
			padding: 10px;
			text-decoration: none;
			border-bottom: 1px solid rgba(0,0,0,0.25);
			text-align: center;
		}

		#menu a:last-child {
			border: none;
		}

		#menu a:hover {
			background-color: #f8f8f8;
			color: #404040;
		}

		#menu a.active {
			background-color: #3887be;
			color: #ffffff;
		}

		#menu a.active:hover {
			background: #3074a4;
		}
	</style>
</head>
<body>
	<?php include 'template/navbar.php'; ?>
	
	<nav id="menu"></nav>
	<div id='map'></div>
	<script>
	mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
	var map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
		center: [140.602, -4], // starting position [lng, lat]
		zoom: 7 // starting zoom
	});
	var nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'top-left');

	map.on('load', function () {

		/*
		//batas_negara_KAB_KEEROM
		map.addLayer({
			'id': 'batas_negara_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/batas_negara_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});
		*/

		//ADMIN_DS_KAB_KEEROM_BPS
		map.addLayer({
			'id': 'ADMIN_DS_KAB_KEEROM_BPS',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ADMIN_DS_KAB_KEEROM_BPS.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});
		

		//ADMIN_DS_KAB_KEEROM_BPS_LN
		map.addLayer({
			"id": "ADMIN_DS_KAB_KEEROM_BPS_LN",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/ADMIN_DS_KAB_KEEROM_BPS_LN.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
			},
			"paint": {
				"line-color": "red",
				"line-width": 3
			}
		});

		//ADMIN_KEC_KAB_KEEROM
		map.addLayer({
			'id': 'ADMIN_KEC_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ADMIN_KEC_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#42bcf4',
				'fill-opacity': 0.8
			}
		});

		//ADMIN_KEC_KAB_KEEROM_BPS_LN
		map.addLayer({
			"id": "ADMIN_KEC_KAB_KEEROM_BPS_LN",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/ADMIN_KEC_KAB_KEEROM_BPS_LN.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#42bcf4",
				"line-width": 3
			}
		});

		//HUTAN_KAB_KEEROM
		map.addLayer({
			'id': 'HUTAN_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/HUTAN_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#34f72a',
				'fill-opacity': 0.8
			}
		});

		//JALAN_KAB_KEEROM_BPS
		map.addLayer({
			"id": "JALAN_KAB_KEEROM_BPS",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/JALAN_KAB_KEEROM_BPS.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#666666",
				"line-width": 3
			}
		});

		//SEBAGIAN_KEC_PROV_PAPUA
		map.addLayer({
			'id': 'SEBAGIAN_KEC_PROV_PAPUA',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/SEBAGIAN_KEC_PROV_PAPUA.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

		//SUNGAI_Keerom
		map.addLayer({
			"id": "SUNGAI_Keerom",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/SUNGAI_Keerom.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#2679ff",
				"line-width": 3
			}
		});
		
		//IBUKOTA_KAB_KEEROM
		/*
		map.addLayer({
			"id": "IBUKOTA_KAB_KEEROM",
			"type": "circle",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/IBUKOTA_KAB_KEEROM.geojson'
			},
			"layout": {
				"visibility" : "visible"
			}
		}); 
		*/
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "IBUKOTA_KAB_KEEROM",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/IBUKOTA_KAB_KEEROM.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		
		//Papua_Keerom_Senggi_Review-Batas-Kawasan-
		map.addLayer({
			'id': 'Papua_Keerom_Senggi_Review-Batas-Kawasan-',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/Papua_Keerom_Senggi_Review-Batas-Kawasan-.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#ff6219',
				'fill-opacity': 0.8
			}
		});

		//Papua_Keerom_Senggi_Review-Batas-SKP-
		map.addLayer({
			'id': 'Papua_Keerom_Senggi_Review-Batas-SKP-',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/Papua_Keerom_Senggi_Review-Batas-SKP-.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#ffad87',
				'fill-opacity': 0.8
			}
		});

		//IDENTIFIKASI-POTENSI-KEEROM
		map.addLayer({
			'id': 'IDENTIFIKASI-POTENSI-KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/IDENTIFIKASI-POTENSI-KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': 'red',
				'fill-opacity': 0.8
			}
		});
		
		//SKP Woslay
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Woslay",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Woslay.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Senggi
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Senggi",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Senggi.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Jabanda
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Jabanda",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Jabanda.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Yuruf
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Yuruf",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Yuruf.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
	});

	var layer = [
		'IBUKOTA_KAB_KEEROM',
		'ADMIN_DS_KAB_KEEROM_BPS', 
		'ADMIN_DS_KAB_KEEROM_BPS_LN', 
		'ADMIN_KEC_KAB_KEEROM', 
		'HUTAN_KAB_KEEROM', 
		'JALAN_KAB_KEEROM_BPS', 
		'SEBAGIAN_KEC_PROV_PAPUA', 
		'SUNGAI_Keerom',
		'IDENTIFIKASI-POTENSI-KEEROM', 
		'Papua_Keerom_Senggi_Review-Batas-Kawasan-', 
		'Papua_Keerom_Senggi_Review-Batas-SKP-'];

	layer.forEach(layer => {
		map.on('click', layer, function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML(layer)
				.addTo(map);
		});
	});

	map.on('click', 'IDENTIFIKASI-POTENSI-KEEROM', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<h6><center>IDENTIFIKASI POTENSI KEEROM</center><br><center>KTM Senggi Telah ditetapkan sebagai kawasan transmigrasi oleh Menteri Desa, PDT dan Transmigrasi berdasarkan SK No. 104 Tahun 2017</center></h6><br><iframe width="1000" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_keerom'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	for (var i = 0; i < layer.length; i++) {
		var id = layer[i];

		var link = document.createElement('a');
		link.href = '#';
		link.className = 'active';
		link.textContent = id;

		link.onclick = function (e) {
			var clickedLayer = this.textContent;
			e.preventDefault();
			e.stopPropagation();

			var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

			if (visibility === 'visible') {
				map.setLayoutProperty(clickedLayer, 'visibility', 'none');
				this.className = '';
			} else {
				this.className = 'active';
				map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
			}
		};

		var layers = document.getElementById('menu');
		layers.appendChild(link);
	}

	//popup SKP Woslay
	map.on('click', 'SKP Woslay', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Woslay<p><ul><li>SP Molof</li><li>SP Wsolay 1</li><li>SP Woslay 2</li><li>SP Woslay 3</li></ul>')
				.addTo(map);
		});
	//popup SKP Senggi
	map.on('click', 'SKP Senggi', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Senggi<p><ul><li>SP Senggi 1</li><li>SP Senggi 2</li><li>SP Senggi 3</li><li>SP Kalifam 1</li><li>SP Kalifam 2</li></ul>')
				.addTo(map);
		});
	//popup SKP Jabanda
	map.on('click', 'SKP Jabanda', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Jabanda<p><ul><li>SP Jabanda 1</li><li>SP Jabanda 2</li><li>SP Yuwainda 1</li><li>SP Warlef 1</li><li>SP Warlef 2</li></ul>')
				.addTo(map);
		});
	//popup SKP Yuruf
	map.on('click', 'SKP Yuruf', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Yuruf<p><ul><li>SP Yuruf 1</li><li>SP Yuruf 2</li><li>SP Dubu</li><li>SP Umuaf</li></ul>')
				.addTo(map);
		});

	//popup data SKP
	//A
	map.on('click', 'SKP Woslay', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP A (Woslay)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_a'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//B
	map.on('click', 'SKP Senggi', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP B (Senggi)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_b'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//C
	map.on('click', 'SKP Jabanda', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP C (Jabanda)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_c'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//D
	map.on('click', 'SKP Yuruf', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP D (Yuruf)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_d'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//KPB
	map.on('click', 'Papua_Keerom_Senggi_Review-Batas-Kawasan-', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP KPB<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_kpb'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});
	</script>
</body>
</html>