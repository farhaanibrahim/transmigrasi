<link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
<table class="table table-bordered table-stripped" id="myTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th></th>
                        <th>No</th>
                        <th>Potensi Transmigrasi</th>
                        <th>Kecamatan</th>
                        <th>Desa</th>
                        <th>Luas APL (Ha)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; ?>
                    <?php foreach($identifikasi->result() as $row): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->ket; ?></td>
                            <td><?php echo $row->no; ?></td>
                            <td><?php echo $row->potensi; ?></td>
                            <td><?php echo $row->kecamatan; ?></td>
                            <td><?php echo $row->desa; ?></td>
                            <td><?php echo $row->luas_apl; ?></td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>