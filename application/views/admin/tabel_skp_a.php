<link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
<table id="myTable" class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th rowspan="2">No. </th>
                        <th rowspan="2">SKP</th>
                        <th rowspan="2">Luas</th>
                        <th colspan="2">Kawasan Hutan</th>
                        <th rowspan="2">Kecamatan</th>
                        <th colspan="2">Desa</th>
                        <th rowspan="2">Pusat SKP/KPB</th>
                        <th rowspan="2">Potensi SP</th>
                        <th rowspan="2">Pola Usaha Pokok</th>
                    </tr>
                    <tr>
                        <th>Klasifikasi</th>
                        <th>Luas</th>
                        <th>Nama</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; ?>
                    <?php foreach($skp->result() as $row): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->nm_skp; ?></td>
                            <td><?php echo $row->luas; ?></td>
                            <td><?php echo $row->kws_htn_klasifikasi; ?></td>
                            <td><?php echo $row->kws_htn_luas; ?></td>
                            <td><?php echo $row->kecamatan; ?></td>
                            <td><?php echo $row->nm_desa; ?></td>
                            <td><?php echo $row->status_desa; ?></td>
                            <td><?php echo $row->pusat_skp_kpb; ?></td>
                            <td><?php echo $row->potensi_sp; ?></td>
                            <td><?php echo $row->pola_ush_pokok; ?></td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>