<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/backend'); ?>/dist/css/skins/skin-blue.min.css">
  <!-- Map Box -->
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css' rel='stylesheet' />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:50px; bottom:50px; left:0; right:0; width:100%; }
    </style>
	<style>
		#menu {
			background: #fff;
			position: absolute;
			z-index: 1;
			top: 60px;
			right: 10px;
			border-radius: 3px;
			width: auto;
			border: 1px solid rgba(0,0,0,0.4);
			font-family: 'Open Sans', sans-serif;
		}

		#menu a {
			font-size: 13px;
			color: #404040;
			display: block;
			margin: 0;
			padding: 0;
			padding: 10px;
			text-decoration: none;
			border-bottom: 1px solid rgba(0,0,0,0.25);
			text-align: center;
		}

		#menu a:last-child {
			border: none;
		}

		#menu a:hover {
			background-color: #f8f8f8;
			color: #404040;
		}

		#menu a.active {
			background-color: #3887be;
			color: #ffffff;
		}

		#menu a.active:hover {
			background: #3074a4;
		}
	</style>
<div class="wrapper">

  <?php include 'template/header.php' ?>
  <?php include 'template/sidebar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
	    <div id='map'></div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Transmigrasi</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/backend'); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/backend'); ?>/dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
<script>
	mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
	var map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
		center: [115.5407517, -0.8780049], // starting position [lng, lat]
		zoom: 4 // starting zoom
	});
	var nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'top-right');

	map.on('load', function () {

		/*
		//batas_negara_KAB_KEEROM
		map.addLayer({
			'id': 'batas_negara_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/batas_negara_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});
		*/

		//ADMIN_DS_KAB_KEEROM_BPS
		map.addLayer({
			'id': 'ADMIN_DS_KAB_KEEROM_BPS',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ADMIN_DS_KAB_KEEROM_BPS.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});
		

		//ADMIN_DS_KAB_KEEROM_BPS_LN
		map.addLayer({
			"id": "ADMIN_DS_KAB_KEEROM_BPS_LN",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/ADMIN_DS_KAB_KEEROM_BPS_LN.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
			},
			"paint": {
				"line-color": "red",
				"line-width": 3
			}
		});

		//ADMIN_KEC_KAB_KEEROM
		map.addLayer({
			'id': 'ADMIN_KEC_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ADMIN_KEC_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#42bcf4',
				'fill-opacity': 0.8
			}
		});

		//ADMIN_KEC_KAB_KEEROM_BPS_LN
		map.addLayer({
			"id": "ADMIN_KEC_KAB_KEEROM_BPS_LN",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/ADMIN_KEC_KAB_KEEROM_BPS_LN.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#42bcf4",
				"line-width": 3
			}
		});

		//HUTAN_KAB_KEEROM
		map.addLayer({
			'id': 'HUTAN_KAB_KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/HUTAN_KAB_KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#34f72a',
				'fill-opacity': 0.8
			}
		});

		//JALAN_KAB_KEEROM_BPS
		map.addLayer({
			"id": "JALAN_KAB_KEEROM_BPS",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/JALAN_KAB_KEEROM_BPS.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#666666",
				"line-width": 3
			}
		});

		//SEBAGIAN_KEC_PROV_PAPUA
		map.addLayer({
			'id': 'SEBAGIAN_KEC_PROV_PAPUA',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/SEBAGIAN_KEC_PROV_PAPUA.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

		//SUNGAI_Keerom
		map.addLayer({
			"id": "SUNGAI_Keerom",
			"type": "line",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/SUNGAI_Keerom.geojson'
			},
			"layout": {
				"line-join": "round",
				"line-cap": "round"
				},
			"paint": {
				"line-color": "#2679ff",
				"line-width": 3
			}
		});
		
		//IBUKOTA_KAB_KEEROM
		/*
		map.addLayer({
			"id": "IBUKOTA_KAB_KEEROM",
			"type": "circle",
			"source": {
				"type": "geojson",
				"data": 'http://localhost/transmigrasi/assets/geojson/IBUKOTA_KAB_KEEROM.geojson'
			},
			"layout": {
				"visibility" : "visible"
			}
		}); 
		*/
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "IBUKOTA_KAB_KEEROM",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/IBUKOTA_KAB_KEEROM.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		
		//Papua_Keerom_Senggi_Review-Batas-Kawasan-
		map.addLayer({
			'id': 'Papua_Keerom_Senggi_Review-Batas-Kawasan-',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/Papua_Keerom_Senggi_Review-Batas-Kawasan-.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#ff6219',
				'fill-opacity': 0.8
			}
		});

		//Papua_Keerom_Senggi_Review-Batas-SKP-
		map.addLayer({
			'id': 'Papua_Keerom_Senggi_Review-Batas-SKP-',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/Papua_Keerom_Senggi_Review-Batas-SKP-.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#ffad87',
				'fill-opacity': 0.8
			}
		});

		//IDENTIFIKASI-POTENSI-KEEROM
		map.addLayer({
			'id': 'IDENTIFIKASI-POTENSI-KEEROM',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/IDENTIFIKASI-POTENSI-KEEROM.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': 'red',
				'fill-opacity': 0.8
			}
		});
		
		//SKP Woslay
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Woslay",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Woslay.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Senggi
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Senggi",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Senggi.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Jabanda
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Jabanda",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Jabanda.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
		//SKP Yuruf
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP Yuruf",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/SKP_Yuruf.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
    //Kalbar_PENETAPAN_Bengkayang_Jagoi Babang Batas SKP 2016
		map.addLayer({
			'id': 'Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/kal_bar/Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016.json'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //kalbar_potensi_ bengkayang
		map.addLayer({
			'id': 'kalbar_potensi_ bengkayang',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/kal_bar/kalbar_potensi_ bengkayang.json'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //kalbar_potensi_bengkayang_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "kalbar_potensi_bengkayang_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/kal_bar/kalbar_potensi_bengkayang_PN.json'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

        //kalbar_skp_kawasan_jagoi_babang_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "kalbar_skp_kawasan_jagoi_babang_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/kal_bar/kalbar_skp_kawasan_jagoi_babang_PN.json'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
    //NTT_PENETAPAN_Belu_Tasufeto-Mandeu_Batas_SKP_2017
		map.addLayer({
			'id': 'NTT_PENETAPAN_Belu_Tasufeto-Mandeu_Batas_SKP_2017',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ntt/NTT_PENETAPAN_Belu_Tasufeto-Mandeu_Batas_SKP_2017.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //NTT_potensi_Belu
		map.addLayer({
			'id': 'NTT_potensi_Belu',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/ntt/NTT_potensi_Belu.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //NTT_potensi_Belu_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "NTT_potensi_Belu_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/ntt/NTT_potensi_Belu_PN.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

        //NTT_skp_kawasan_tasifeto_mandeu_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "NTT_skp_kawasan_tasifeto_mandeu_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/kal_bar/NTT_skp_kawasan_tasifeto_mandeu_PN.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
    //Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)
		map.addLayer({
			'id': 'Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/sul_sel/Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP).geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //SULSEL_potensi_sidrap
		map.addLayer({
			'id': 'SULSEL_potensi_sidrap',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/sul_sel/SULSEL_potensi_sidrap.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //SULSEL_potensi_Sidrap_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SULSEL_potensi_Sidrap_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/sul_sel/SULSEL_potensi_Sidrap_PN.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

        //SULSEL_skp_kawasan_pitu_riase_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SULSEL_skp_kawasan_pitu_riase_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/sul_sel/SULSEL_skp_kawasan_pitu_riase_PN.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
	});

  var layer = [
		'IBUKOTA_KAB_KEEROM',
		'ADMIN_DS_KAB_KEEROM_BPS', 
		'ADMIN_DS_KAB_KEEROM_BPS_LN', 
		'ADMIN_KEC_KAB_KEEROM', 
		'HUTAN_KAB_KEEROM', 
		'JALAN_KAB_KEEROM_BPS', 
		'SEBAGIAN_KEC_PROV_PAPUA', 
		'SUNGAI_Keerom',
		'IDENTIFIKASI-POTENSI-KEEROM', 
		'Papua_Keerom_Senggi_Review-Batas-Kawasan-', 
		'Papua_Keerom_Senggi_Review-Batas-SKP-',
		'Kalbar_PENETAPAN_Bengkayang_Jagoi_Babang_Batas_SKP_2016',
    	'kalbar_potensi_ bengkayang',
    	'kalbar_potensi_bengkayang_PN',
    	'kalbar_skp_kawasan_jagoi_babang_PN',
		'NTT_PENETAPAN_Belu_Tasufeto-Mandeu_Batas_SKP_2017',
    	'NTT_potensi_Belu',
    	'NTT_potensi_Belu_PN',
    	'NTT_skp_kawasan_tasifeto_mandeu_PN',
		'Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)',
   		'SULSEL_potensi_sidrap',
    	'SULSEL_potensi_Sidrap_PN',
    	'SULSEL_skp_kawasan_pitu_riase_PN'];

	layer.forEach(layer => {
		map.on('click', layer, function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML(layer)
				.addTo(map);
		});
	});

	map.on('click', 'IDENTIFIKASI-POTENSI-KEEROM', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KEEROM<br><iframe width="650" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_keerom'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

  map.on('click', 'kalbar_potensi_bengkayang_PN', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KAB. BENGKAYANG<br><iframe width="800" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_bengkayang'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

  map.on('click', 'NTT_potensi_Belu_PN', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KAB. BELU<br><iframe width="800" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_belu'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

  map.on('click', 'SULSEL_potensi_Sidrap_PN', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//popup SKP Woslay
	map.on('click', 'SKP Woslay', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Woslay<p><ul><li>SP Molof</li><li>SP Wsolay 1</li><li>SP Woslay 2</li><li>SP Woslay 3</li></ul>')
				.addTo(map);
		});
	//popup SKP Senggi
	map.on('click', 'SKP Senggi', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Senggi<p><ul><li>SP Senggi 1</li><li>SP Senggi 2</li><li>SP Senggi 3</li><li>SP Kalifam 1</li><li>SP Kalifam 2</li></ul>')
				.addTo(map);
		});
	//popup SKP Jabanda
	map.on('click', 'SKP Jabanda', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Jabanda<p><ul><li>SP Jabanda 1</li><li>SP Jabanda 2</li><li>SP Yuwainda 1</li><li>SP Warlef 1</li><li>SP Warlef 2</li></ul>')
				.addTo(map);
		});
	//popup SKP Yuruf
	map.on('click', 'SKP Yuruf', function (e) {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('<p>SKP Yuruf<p><ul><li>SP Yuruf 1</li><li>SP Yuruf 2</li><li>SP Dubu</li><li>SP Umuaf</li></ul>')
				.addTo(map);
		});

	//popup data SKP
	//A
	map.on('click', 'SKP Woslay', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP A (Woslay)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_a'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//B
	map.on('click', 'SKP Senggi', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP B (Senggi)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_b'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//C
	map.on('click', 'SKP Jabanda', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP C (Jabanda)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_c'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//D
	map.on('click', 'SKP Yuruf', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP D (Yuruf)<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_d'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	//KPB
	map.on('click', 'Papua_Keerom_Senggi_Review-Batas-Kawasan-', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP KPB<br><iframe width="1000" height="400" src="<?php echo base_url('admin/tabel_skp_kpb'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});
	</script>