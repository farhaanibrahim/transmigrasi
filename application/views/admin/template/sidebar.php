<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview">
      <a href="#"><i class="fa fa-file"></i> <span>Data Identifikasi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
    <li><a href="<?php echo base_url('admin/data_identifikasi'); ?>"><i class="fa fa-file-excel-o"></i> <span>Identifikasi Kab. Keerom</span></a></li>
    <li><a href="<?php echo base_url('admin/identifikasi_kab_bengkayang'); ?>"><i class="fa fa-file-excel-o"></i> <span>Identifikasi Kab. bengkayang</span></a></li>
    <li><a href="<?php echo base_url('admin/identifikasi_kab_belu'); ?>"><i class="fa fa-file-excel-o"></i> <span>Identifikasi Kab. Belu</span></a></li>
    <li><a href="<?php echo base_url('admin/identifikasi_kab_sidrap'); ?>"><i class="fa fa-file-excel-o"></i> <span>Identifikasi Kab. Sidrap</span></a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-file"></i> <span>Data SKP Kab. Senggi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo base_url(); ?>admin/skp_a">SKP A (WOSLAY)</a></li>
        <li><a href="<?php echo base_url(); ?>admin/skp_b">SKP B (SENGGI)</a></li>
        <li><a href="<?php echo base_url(); ?>admin/skp_c">SKP C (JABANDA)</a></li>
        <li><a href="<?php echo base_url(); ?>admin/skp_d">SKP D (YURUF)</a></li>
        <li><a href="<?php echo base_url(); ?>admin/skp_kpb">SKP KPB</a></li>
      </ul>
    </li>
    <li><a href="<?php echo base_url('admin/skp_bengkayang'); ?>"><i class="fa fa-file"></i> <span>Data SKP Kab. Bengkayang</span></a></li>
    <li><a href="<?php echo base_url('admin/skp_belu'); ?>"><i class="fa fa-file"></i> <span>Data SKP Kab. Belu</span></a></li>
    <li><a href="<?php echo base_url('admin/skp_sidenreng_rappang'); ?>"><i class="fa fa-file"></i> <span>Data SKP Kab. Sidenreng Rappang</span></a></li>
    <li><a href="<?php echo base_url('admin/management_user'); ?>"><i class="fa fa-user"></i> <span>Management User</span></a></li>
  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>