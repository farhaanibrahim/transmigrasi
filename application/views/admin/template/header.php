<!-- Main Header -->
<header class="main-header">

<!-- Logo -->
<a href="<?php echo base_url('map'); ?>" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><img src="<?php echo base_url(); ?>/assets/logo/Kemendes_Logo.png" width="30" height="30" class="d-inline-block align-top" alt=""></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>Transmigrasi</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account Menu -->
      <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span class="hidden-xs"><?php echo $this->session->userdata('username'); ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo base_url(); ?>/assets/logo/Kemendes_Logo.png" class="img-circle" alt="User Image">

            <p>
              <?php echo $this->session->userdata('username'); ?>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="<?php echo base_url('admin/edit_profile'); ?>/<?php echo $this->session->userdata('id_user'); ?>" class="btn btn-default btn-flat">Edit Profile</a>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>