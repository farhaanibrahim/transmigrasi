<!--
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	<ul class="navbar-nav">
		<li class="nav-item active">
		<a class="nav-link" href="#">Map</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map'); ?>">Papua</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/kal_bar'); ?>">Kalimantan Barat</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/ntt'); ?>">NTT</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/sulsel'); ?>">Sulawesi Selatan</a>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li class=""><a class="nav-link btn btn-sm btn-danger" href="<?php echo base_url('admin'); ?>">Admin</a></li>
	</ul>
</nav>
-->
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">
  	<img src="<?php echo base_url(); ?>/assets/logo/Kemendes_Logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
  	Transmigrasi
  </a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map'); ?>">Papua</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/kal_bar'); ?>">Kalimantan Barat</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/ntt'); ?>">NTT</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('map/sulsel'); ?>">Sulawesi Selatan</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('admin'); ?>">Data</a>
		</li>
    </ul>
    <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-danger my-2 my-sm-0" type="submit">Sign Out</a>
  </div>
</nav>