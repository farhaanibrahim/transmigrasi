<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/login'); ?>/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login'); ?>/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
		<div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('<?php echo base_url('assets/login'); ?>/images/bg01.jpg');">
			
			<div class="p-t-50 p-b-60" style="margin-top: 250px;">
				<p class="m1-txt1 p-b-36" style="color: white;">
				Sign in to start your session
				</p>

				<form class="contact100-form validate-form" action="<?php echo base_url('login/loginProcess'); ?>" method="post" enctype="multipart/form-data">
					<div class="wrap-input100 m-b-10 validate-input" data-validate = "Name is required">
						<input class="s2-txt1 placeholder0 input100" type="text" name="username" placeholder="Your Username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 m-b-20 validate-input" data-validate = "Email is required: ex@abc.xyz">
						<input class="s2-txt1 placeholder0 input100" type="password" name="password" placeholder="Your Password">
						<span class="focus-input100"></span>
					</div>

					<p><?php echo $img; ?></p>
					<div class="wrap-input100 m-b-20 validate-input" data-validate = "Captcha is required">
							<input class="s2-txt1 placeholder0 input100" type="text" name="captcha">
							<input type="hidden" name="captword" value="<?php echo $captword ?>">
							<span class="focus-input100"></span>
					</div>

					<div class="w-full">
						<input type="submit" name="btnSubmit" class="flex-c-m s2-txt2 size4 bg1 bor1 hov1 trans-0" value="Login">
					</div>
				</form>
				<br><br>
				<a class="s2-txt3 p-t-18" href="<?php echo base_url('login/register'); ?>">
					Register
				</a>
			</div>
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">

			<div class="wrap-pic1">
				<img src="<?php echo base_url(); ?>/assets/logo/Kemendes_Logo.png" alt="LOGO">
			</div>

			<div class="p-t-50 p-b-60">
				<p class="m1-txt1 p-b-36">
				<h1>KEMENTERIAN DESA, PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI</h1>
				
				</p>
			</div>
		</div>
	</div>



	

<!--===============================================================================================-->	
	<script src="<?php echo base_url('assets/login'); ?>/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/login'); ?>/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('assets/login'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/login'); ?>/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/login'); ?>/vendor/countdowntime/moment.min.js"></script>
	<script src="<?php echo base_url('assets/login'); ?>/vendor/countdowntime/moment-timezone.min.js"></script>
	<script src="<?php echo base_url('assets/login'); ?>/vendor/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="<?php echo base_url('assets/login'); ?>/vendor/countdowntime/countdowntime.js"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 35,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/login'); ?>/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/login'); ?>/js/main.js"></script>

</body>
</html>