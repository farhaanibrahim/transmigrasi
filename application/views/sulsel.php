<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kabupaten Sindereng Rappang</title>
    <!-- Map Box -->
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css' rel='stylesheet' />

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:50px; bottom:0; width:100%; }
    </style>
    <style>
		#menu {
			background: #fff;
			position: absolute;
			z-index: 1;
			top: 60px;
			right: 10px;
			border-radius: 3px;
			width: auto;
			border: 1px solid rgba(0,0,0,0.4);
			font-family: 'Open Sans', sans-serif;
		}

		#menu a {
			font-size: 13px;
			color: #404040;
			display: block;
			margin: 0;
			padding: 0;
			padding: 10px;
			text-decoration: none;
			border-bottom: 1px solid rgba(0,0,0,0.25);
			text-align: center;
		}

		#menu a:last-child {
			border: none;
		}

		#menu a:hover {
			background-color: #f8f8f8;
			color: #404040;
		}

		#menu a.active {
			background-color: #3887be;
			color: #ffffff;
		}

		#menu a.active:hover {
			background: #3074a4;
		}
	</style>
</head>
<body>
<?php include 'template/navbar.php'; ?>
    <nav id="menu"></nav>
    <div id='map'></div>
</body>
</html>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
	var map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
		center: [120.05, -3.7], // starting position [lng, lat]
		zoom: 10 // starting zoom
	});
	var nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'top-left');

    map.on('load', function() {
        //Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)
		map.addLayer({
			'id': 'Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/sul_sel/Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP).geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //SULSEL_potensi_sidrap
		map.addLayer({
			'id': 'SULSEL_potensi_sidrap',
			'type': 'fill',
			'source': {
				'type': 'geojson',
				'data': 'http://localhost/transmigrasi/assets/geojson/sul_sel/SULSEL_potensi_sidrap.geojson'
			},
			'layout': {},
			'paint': {
				'fill-color': '#666666',
				'fill-opacity': 0.8
			}
		});

        //SULSEL_potensi_Sidrap_PN
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SULSEL_potensi_Sidrap_PN",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": 'http://localhost/transmigrasi/assets/geojson/sul_sel/SULSEL_potensi_Sidrap_PN.geojson'
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

        //SKP A Sindenrappang
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP A Sindenrappang",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "skp": "SKP A" }, "geometry": { "type": "Point", "coordinates": [ 119.980581662310414, -3.77329758035367 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP B Sindenrappang
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP B Sindenrappang",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "skp": "SKP B" }, "geometry": { "type": "Point", "coordinates": [ 120.035106728063397, -3.64331377249851 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP C Sindenrappang
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP C Sindenrappang",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "skp": "SKP C" }, "geometry": { "type": "Point", "coordinates": [ 120.156427633383856, -3.741125859103453 ] } },
								{ "type": "Feature", "properties": { "Id": 0, "skp": "SKP C" }, "geometry": { "type": "Point", "coordinates": [ 120.300153271708012, -3.676335222396642 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});

		//SKP KPB Sindenrappang
		map.loadImage('http://localhost/transmigrasi/assets/symbol/marker.png', function(error, image) {
			if (error) throw error;
			map.addImage('marker', image);
			map.addLayer({
				"id": "SKP KPB Sindenrappang",
				"type": "symbol",
				"source": {
					"type": "geojson",
					"data": {
						"type": "FeatureCollection",
						"name": "skp_bengkayang",
						"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
							"features": [
								{ "type": "Feature", "properties": { "Id": 0, "skp": "KPB" }, "geometry": { "type": "Point", "coordinates": [ 120.043750367442769, -3.770568030624561 ] } }
							]
					}
				},
				"layout": {
					"icon-image": "marker",
					"icon-size": 0.05
				}
			});
		});
    });

    var layer = [
        'Sulsel_PENETAPAN_Sindenrappang_Pituriase (Batas SKP)',
        'SULSEL_potensi_sidrap',
        'SULSEL_potensi_Sidrap_PN',
		'SKP A Sindenrappang',
		'SKP B Sindenrappang',
		'SKP C Sindenrappang',
		'SKP KPB Sindenrappang'
    ];

	layer.forEach(layer => {
		map.on('click', layer, function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML(layer)
				.addTo(map);
		});
	});

	map.on('click', 'SULSEL_potensi_Sidrap_PN', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('IDENTIFIKASI POTENSI KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/data_identifikasi_kab_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP A Sindenrappang', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP A Sindenrappang KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_a_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP B Sindenrappang', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP B Sindenrappang KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_b_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP C Sindenrappang', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP C Sindenrappang KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_c_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

	map.on('click', 'SKP KPB Sindenrappang', function (e) {
			console.log(e.features[0].properties.length);
				
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML('SKP KPB Sindenrappang KAB. SIDRAP<br><iframe width="800" height="400" src="<?php echo base_url('admin/tabel_skp_kpb_sidrap'); ?>" allowfullscreen></iframe>')
				.addTo(map);
	});

    for (var i = 0; i < layer.length; i++) {
		var id = layer[i];

		var link = document.createElement('a');
		link.href = '#';
		link.className = 'active';
		link.textContent = id;

		link.onclick = function (e) {
			var clickedLayer = this.textContent;
			e.preventDefault();
			e.stopPropagation();

			var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

			if (visibility === 'visible') {
				map.setLayoutProperty(clickedLayer, 'visibility', 'none');
				this.className = '';
			} else {
				this.className = 'active';
				map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
			}
		};

		var layers = document.getElementById('menu');
		layers.appendChild(link);
	}
</script>