<?php
class Admin extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('AppModel');
        $this->load->library('excel');
    }

    public function index()
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('admin/dashboard');
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function data_identifikasi()
    {
        if ($this->session->userdata('status')=='login') {
            $data['identifikasi'] = $this->AppModel->get_identifikasi();
            $this->load->view('admin/data_identifikasi',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function import_identifikasi()
    {
        if ($this->session->userdata('status')=='login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size'] = '10000';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_identifikasi')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
				    redirect(base_url('admin/import_identifikasi'));
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $extension = $upload_data['file_ext'];

                    if ($extension == '.xlsx') {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    } else {
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load(FCPATH.'upload/'.$file_name);
                    $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                    $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

                    $this->AppModel->truncate('t_id_keerom');

					for ($i=4; $i <= $totalrows; $i++) {
						$data = array(
							'id'=>'',
							'ket'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							'no'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							'potensi'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
							'kecamatan'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
							'desa'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							'luas_apl'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' '
						);
						$this->AppModel->insert_identifikasi($data);
                    }
                    $this->load->helper('file');
                    delete_files('./upload/');

                    $this->session->set_flashdata("success","Successfully Uploaded!");
                    redirect(base_url('admin/data_identifikasi'));
                }
                
            } else {
                $this->load->view('admin/import_identifikasi');
            }
            
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function identifikasi_kab_bengkayang()
    {
        if ($this->session->userdata('status')=='login') {
            $data['identifikasi'] = $this->AppModel->get_id_bengkayang();
            $this->load->view('admin/data_id_bengkayang',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function identifikasi_kab_belu()
    {
        if ($this->session->userdata('status')=='login') {
            $data['identifikasi'] = $this->AppModel->get_id_belu();
            $this->load->view('admin/data_id_belu',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function identifikasi_kab_sidrap()
    {
        if ($this->session->userdata('status')=='login') {
            $data['identifikasi'] = $this->AppModel->get_id_sidrap();
            $this->load->view('admin/data_id_sidrap',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function import_id_bengkayang()
    {
        if ($this->session->userdata('status')=='login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size'] = '10000';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_identifikasi')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
				    redirect(base_url('admin/import_id_bengkayang'));
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $extension = $upload_data['file_ext'];

                    if ($extension == '.xlsx') {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    } else {
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load(FCPATH.'upload/'.$file_name);
                    $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                    $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

                    $this->AppModel->truncate('t_id_bengkayang');

					for ($i=4; $i <= $totalrows; $i++) {
						$data = array(
							'id'=>'',
							'ket'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							'no'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							'potensi'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
							'kecamatan'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
							'desa'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							'luas_apl'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' '
						);
						$this->AppModel->insert_id_bengkayang($data);
                    }
                    $this->load->helper('file');
                    delete_files('./upload/');

                    $this->session->set_flashdata("success","Successfully Uploaded!");
                    redirect(base_url('admin/identifikasi_kab_bengkayang'));
                }
                
            } else {
                $this->load->view('admin/import_id_bengkayang');
            }
            
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function import_id_belu()
    {
        if ($this->session->userdata('status')=='login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size'] = '10000';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_identifikasi')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
				    redirect(base_url('admin/import_id_belu'));
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $extension = $upload_data['file_ext'];

                    if ($extension == '.xlsx') {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    } else {
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load(FCPATH.'upload/'.$file_name);
                    $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                    $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

                    $this->AppModel->truncate('t_id_belu');

					for ($i=4; $i <= $totalrows; $i++) {
						$data = array(
							'id'=>'',
							'ket'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							'no'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							'potensi'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
							'kecamatan'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
							'desa'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							'luas_apl'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' '
						);
						$this->AppModel->insert_id_belu($data);
                    }
                    $this->load->helper('file');
                    delete_files('./upload/');

                    $this->session->set_flashdata("success","Successfully Uploaded!");
                    redirect(base_url('admin/identifikasi_kab_belu'));
                }
                
            } else {
                $this->load->view('admin/import_id_belu');
            }
            
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function import_id_sidrap()
    {
        if ($this->session->userdata('status')=='login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size'] = '10000';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_identifikasi')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
				    redirect(base_url('admin/import_id_sidrap'));
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $extension = $upload_data['file_ext'];

                    if ($extension == '.xlsx') {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    } else {
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load(FCPATH.'upload/'.$file_name);
                    $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                    $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);

                    $this->AppModel->truncate('t_id_sidrap');

					for ($i=4; $i <= $totalrows; $i++) {
						$data = array(
							'id'=>'',
							'ket'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							'no'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							'potensi'=>$objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(3,$i)->getValue() : ' ',
							'kecamatan'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
							'desa'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							'luas_apl'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' '
						);
						$this->AppModel->insert_id_sidrap($data);
                    }
                    $this->load->helper('file');
                    delete_files('./upload/');

                    $this->session->set_flashdata("success","Successfully Uploaded!");
                    redirect(base_url('admin/identifikasi_kab_sidrap'));
                }
                
            } else {
                $this->load->view('admin/import_id_sidrap');
            }
            
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_a()
    {
        if ($this->session->userdata('status')=='login') {
            $skp = 'a';
            $data['skp'] = $this->AppModel->get_skp($skp);
            $this->load->view('admin/data_skp_a',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function import_data_skp()
    {
        if ($this->session->userdata('status')=='login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'xls|xlsx';
                $config['max_size'] = '10000';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file_skp')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
				    redirect(base_url('admin/import_data_skp'));
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $extension = $upload_data['file_ext'];

                    if ($extension == '.xlsx') {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    } else {
                        $objReader = PHPExcel_IOFactory::createReader('Excel5');
                    }

                    $objReader->setReadDataOnly(true);

                    $objPHPExcel = $objReader->load(FCPATH.'upload/'.$file_name);
                    $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                    $objWorkSheet = $objPHPExcel->setActiveSheetIndex(0);
                    
                    $skp = $this->input->post('skp');
                    $this->AppModel->truncate_skp('t_skp',$skp);

					for ($i=17; $i <= $totalrows; $i++) {
						$data = array(
                            'id'=>'',
                            'skp'=>$skp,
							'nm_skp'=>$objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(1,$i)->getValue() : ' ',
							'luas'=>$objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(2,$i)->getValue() : ' ',
							'kws_htn_klasifikasi'=>$objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(4,$i)->getValue() : ' ',
							'kws_htn_luas'=>$objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(5,$i)->getValue() : ' ',
							'kecamatan'=>$objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(6,$i)->getValue() : ' ',
                            'nm_desa'=>$objWorkSheet->getCellByColumnAndRow(9,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(9,$i)->getValue() : ' ',
                            'status_desa'=>$objWorkSheet->getCellByColumnAndRow(10,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(10,$i)->getValue() : ' ',
                            'pusat_skp_kpb'=>$objWorkSheet->getCellByColumnAndRow(13,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(13,$i)->getValue() : ' ',
                            'potensi_sp'=>$objWorkSheet->getCellByColumnAndRow(14,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(14,$i)->getValue() : ' ',
                            'pola_ush_pokok'=>$objWorkSheet->getCellByColumnAndRow(15,$i)->getValue() ? $objWorkSheet->getCellByColumnAndRow(15,$i)->getValue() : ' '
						);
						$this->AppModel->insert_skp($data);
                    }
                    $this->load->helper('file');
                    delete_files('./upload/');

                    $this->session->set_flashdata("success","Successfully Uploaded!");
                    redirect(base_url('admin/import_data_skp'));
                }
            } else {
                $this->load->view('admin/import_data_skp');
            }
            
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_b()
    {
        if ($this->session->userdata('status')=='login') {
            $skp = 'b';
            $data['skp'] = $this->AppModel->get_skp($skp);
            $this->load->view('admin/data_skp_b',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_c()
    {
        if ($this->session->userdata('status')=='login') {
            $skp = 'c';
            $data['skp'] = $this->AppModel->get_skp($skp);
            $this->load->view('admin/data_skp_c',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_d()
    {
        if ($this->session->userdata('status')=='login') {
            $skp = 'd';
            $data['skp'] = $this->AppModel->get_skp($skp);
            $this->load->view('admin/data_skp_d',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_kpb()
    {
        if ($this->session->userdata('status')=='login') {
            $skp = 'kpb';
            $data['skp'] = $this->AppModel->get_skp($skp);
            $this->load->view('admin/data_skp_kpb',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function management_user()
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('template/underconstruction');
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function edit_profile($id)
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('template/underconstruction');
        } else {
            redirect(base_url('login'));
        }
    }

    public function skp_bengkayang()
    {
        if ($this->session->userdata('status') == 'login') {
            $data['skp'] = $this->AppModel->get_skp_bengkayang();
            $this->load->view('admin/skp_bengkayang',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function tambah_skp_bengkayang()
    {
        if ($this->session->userdata('status') == 'login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './upload/';
                $config['allowed_types']        = 'xls|xlsx';
                $config['max_size']             = 10000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('skp'))
                {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect(base_url('admin/tambah_skp_bengkayang'));
                }
                else
                {    
                    $data = array(
                        'id'=>'',
                        'skp'=>$this->input->post('skp'),
                        'file'=>$this->upload->data('file_name'),
                        'tgl'=>date('Ymd')
                    ); 
                    $this->AppModel->insert_skp_bengkayang($data);
                    $this->session->set_flashdata('success', 'File uploaded');
                    redirect(base_url('admin/skp_bengkayang'));
                }
            } else {
                $this->load->view('admin/tambah_skp_bengkayang');
            }
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_belu()
    {
        if ($this->session->userdata('status') == 'login') {
            $data['skp'] = $this->AppModel->get_skp_belu();
            $this->load->view('admin/skp_belu',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function tambah_skp_belu()
    {
        if ($this->session->userdata('status') == 'login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './upload/';
                $config['allowed_types']        = 'xls|xlsx';
                $config['max_size']             = 10000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('skp'))
                {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect(base_url('admin/tambah_skp_belu'));
                }
                else
                {    
                    $data = array(
                        'id'=>'',
                        'skp'=>$this->input->post('skp'),
                        'file'=>$this->upload->data('file_name'),
                        'tgl'=>date('Ymd')
                    ); 
                    $this->AppModel->insert_skp_belu($data);
                    $this->session->set_flashdata('success', 'File uploaded');
                    redirect(base_url('admin/skp_belu'));
                }
            } else {
                $this->load->view('admin/tambah_skp_belu');
            }
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function skp_sidenreng_rappang()
    {
        if ($this->session->userdata('status') == 'login') {
            $data['skp'] = $this->AppModel->get_skp_sidenreng_rappang();
            $this->load->view('admin/skp_sidenreng_rappang',$data);
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function tambah_skp_sidenreng_rappang()
    {
        if ($this->session->userdata('status') == 'login') {
            if (isset($_POST['btnSubmit'])) {
                $config['upload_path']          = './upload/';
                $config['allowed_types']        = 'xls|xlsx';
                $config['max_size']             = 10000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('skp'))
                {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect(base_url('admin/tambah_skp_sidenreng_rappang'));
                }
                else
                {    
                    $data = array(
                        'id'=>'',
                        'skp'=>$this->input->post('skp'),
                        'file'=>$this->upload->data('file_name'),
                        'tgl'=>date('Ymd')
                    ); 
                    $this->AppModel->insert_skp_sidenreng_rappang($data);
                    $this->session->set_flashdata('success', 'File uploaded');
                    redirect(base_url('admin/skp_sidenreng_rappang'));
                }
            } else {
                $this->load->view('admin/tambah_skp_sidenreng_rappang');
            }
        } else {
            redirect(base_url('login'));
        }
        
    }

    public function excel_reader($file)
    {   
        $data = FCPATH."/upload/$file";

        $this->load->library('spreadsheet_excel_reader');

        $this->spreadsheet_excel_reader->setOutputEncoding('CP1251');

        $this->spreadsheet_excel_reader->read($data);

        $sheets = $this->spreadsheet_excel_reader->sheets[0];

        error_reporting(E_ALL ^ E_NOTICE);
        /*
        
        looking for ways to push data into an array
        for ($i = 1; $i <= $sheets['numRows']; $i++) {
            for ($j = 1; $j <= $sheets['numCols']; $j++) {
                echo $sheets['cells'][$i][$j]." ";
            }
            
        }
        */
        echo "<link rel='stylesheet' href='http://localhost/transmigrasi/assets/backend/bower_components/bootstrap/dist/css/bootstrap.min.css'>";
        echo "<table class='table table-bordered table-stripped'>";    
        $x=1;
        while($x<=$sheets['numRows']) {
        echo "\t<tr>\n";
        $y=1;
        while($y<=$sheets['numCols']) {
            $cell = isset($sheets['cells'][$x][$y]) ? $sheets['cells'][$x][$y] : '';
            echo "\t\t<td>$cell</td>\n";  
            $y++;
        }  
        echo "\t</tr>\n";
        $x++;
        }  
        echo "</table>";
        die();
    }

    //For iframe popup mapbox
    public function data_identifikasi_kab_keerom()
    {
        $data['identifikasi'] = $this->AppModel->get_identifikasi();
        $this->load->view('admin/id_kab_keerom',$data);
    }

    public function data_identifikasi_kab_bengkayang()
    {
        $data['identifikasi'] = $this->AppModel->get_id_bengkayang();
        $this->load->view('admin/id_kab_bengkayang',$data);
    }

    public function data_identifikasi_kab_belu()
    {
        $data['identifikasi'] = $this->AppModel->get_id_belu();
        $this->load->view('admin/id_kab_belu',$data);
    }

    public function data_identifikasi_kab_sidrap()
    {
        $data['identifikasi'] = $this->AppModel->get_id_sidrap();
        $this->load->view('admin/id_kab_sidrap',$data);
    }

    public function tabel_skp_a()
    {
        $skp = 'a';
        $data['skp'] = $this->AppModel->get_skp($skp);
        $this->load->view('admin/tabel_skp_a',$data);
    }

    public function tabel_skp_b()
    {
        $skp = 'b';
        $data['skp'] = $this->AppModel->get_skp($skp);
        $this->load->view('admin/tabel_skp_b',$data);
    }
    public function tabel_skp_c()
    {
        $skp = 'c';
        $data['skp'] = $this->AppModel->get_skp($skp);
        $this->load->view('admin/tabel_skp_b',$data);
    }
    public function tabel_skp_d()
    {
        $skp = 'd';
        $data['skp'] = $this->AppModel->get_skp($skp);
        $this->load->view('admin/tabel_skp_b',$data);
    }

    public function tabel_skp_kpb()
    {
        $skp = 'kpb';
        $data['skp'] = $this->AppModel->get_skp($skp);
        $this->load->view('admin/tabel_skp_kpb',$data);
    }

    public function tabel_skp_a_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('A');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_b_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('B');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_c_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('C');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_d_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('D');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_e_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('E');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_f_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('F');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_g_bengkayang()
    {
        $data = $this->AppModel->get_skp_bengkayang_by_skp('G');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_a_belu()
    {
        $data = $this->AppModel->get_skp_belu_by_skp('A');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_b_belu()
    {
        $data = $this->AppModel->get_skp_belu_by_skp('B');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_c_belu()
    {
        $data = $this->AppModel->get_skp_belu_by_skp('C');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_d_belu()
    {
        $data = $this->AppModel->get_skp_belu_by_skp('D');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_kpb_belu()
    {
        $data = $this->AppModel->get_skp_belu_by_skp('KPB');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_a_sidrap()
    {
        $data = $this->AppModel->get_skp_sidrap_by_skp('A');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_b_sidrap()
    {
        $data = $this->AppModel->get_skp_sidrap_by_skp('B');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_c_sidrap()
    {
        $data = $this->AppModel->get_skp_sidrap_by_skp('C');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }

    public function tabel_skp_kpb_sidrap()
    {
        $data = $this->AppModel->get_skp_sidrap_by_skp('KPB');
        foreach ($data->result() as $row) {
            $file = $row->file;
        }
        return $this->excel_reader($file);
    }
    //EOF For iframe popup mapbox

}
