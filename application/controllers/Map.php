<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('status') !== 'login') {
			redirect(base_url('login'));
		}	
	}

	public function index()
	{
		$this->load->view('index');
	}

	public function kal_bar()
	{
		$this->load->view('kal_bar');
	}

	public function ntt()
	{
		$this->load->view('ntt');
	}

	public function sulsel()
	{
		$this->load->view('sulsel');
	}
}
