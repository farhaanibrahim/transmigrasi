<?php
/**
 *
 */
class login extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('AppModel');
  }

  public function Index()
  {
    if ($this->session->userdata('status') == 'login') {
      redirect(base_url('admin'));
    } else {
      $vals = array(
        'word'          => '',
        'img_path'      => './captcha/',
        'img_url'       => base_url('captcha'),
        'font_path'     => './path/to/fonts/texb.ttf',
        'img_width'     => '640',
        'img_height'    => 50,
        'expiration'    => 7200,
        'word_length'   => 8,
        'font_size'     => 16,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  
        // White background and border, black text and red grid
        'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
      );
  
      $cap = create_captcha($vals);
      $data['img'] = $cap['image'];
      $data['captword'] = $cap['word'];

      $this->load->view('login',$data);
    }
  }

  public function loginProcess()
  {
    if ($this->session->userdata('status') == 'login') {
      redirect(base_url('map'));
    } else {
      if ($this->input->post('captcha') == $this->input->post('captword')) {
        $username = $this->input->post('username');
        $password = md5(sha1($this->input->post('password')));

        $data = array(
          'username'=>$username,
          'password'=>$password
        );
        $query = $this->AppModel->login($data);
        if ($query->num_rows() > 0) {
          foreach ($query->result() as $row) {
            if ($row->username == $username && $row->password == $password) {
              $session = array(
                'id_user'=>$row->id,
                'username'=>$row->username,
                'status'=>'login'
              );
              $this->session->set_userdata($session);
              $this->load->helper('file');
              delete_files('./captcha/');
              redirect(base_url('map'));
            } else {
              $this->session->set_flashdata('error','Wrong username or password!');
              redirect(base_url('login'));
            }
          }
        } else {
          $this->session->set_flashdata('error','Wrong username or password!');
          redirect(base_url('login'));
        }
      } else {
        $this->session->set_flashdata('error','Captcha Wrong!');
        redirect(base_url('login'));
      }
    }
  }

  public function logout()
  {
    if ($this->session->userdata('status') == 'login') {
      $this->session->sess_destroy();
      redirect(base_url('login'));
    } else {
      redirect(base_url('login'));
    }
  }

  public function register()
  {
    $this->load->view('register');
  }

  public function register_process()
  {
      $username = $this->input->post('username');
      $password = md5(sha1($this->input->post('password')));
      $retype_password = md5(sha1($this->input->post('retype_password')));

      if ($retype_password == $password) {
          $data = array(
              'id'=>'',
              'username'=>$username,
              'password'=>$password
          );
          $this->AppModel->insert_user($data);
          $this->session->set_flashdata('success',"Registrastion Successfull");
          redirect(base_url('login/register'));
      } else {
          $this->session->set_flashdata('notif',"Re-type password doesn't match");
          redirect(base_url('login/register'));
      }
      
  }
}
