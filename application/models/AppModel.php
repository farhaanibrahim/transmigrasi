<?php
class AppModel extends CI_Model
{
    public function insert_user($data)
    {
        $this->db->insert('t_user',$data);
    }

    public function login($data)
    {
        $this->db->where($data);
        return $this->db->get('t_user');
    }

    public function get_identifikasi()
    {
        return $this->db->get('t_id_keerom');
    }

    public function truncate($table)
    {
        $this->db->truncate($table);
    }

    public function truncate_skp($table,$skp)
    {
        $this->db->where('skp',$skp);
        $this->db->delete($table);
    }

    public function insert_identifikasi($data)
    {
        $this->db->insert('t_id_keerom',$data);
    }

    public function get_id_bengkayang()
    {
        return $this->db->get('t_id_bengkayang');
    }

    public function get_id_belu()
    {
        return $this->db->get('t_id_belu');
    }

    public function get_id_sidrap()
    {
        return $this->db->get('t_id_sidrap');
    }

    public function insert_id_bengkayang($data)
    {
        $this->db->insert('t_id_bengkayang',$data);
    }

    public function insert_id_belu($data)
    {
        $this->db->insert('t_id_belu',$data);
    }

    public function insert_id_sidrap($data)
    {
        $this->db->insert('t_id_sidrap',$data);
    }

    public function get_skp($skp)
    {
        $this->db->where('skp',$skp);
        return $this->db->get('t_skp');
    }

    public function insert_skp($data)
    {
        $this->db->insert('t_skp',$data);
    }

    public function get_skp_bengkayang()
    {
        return $this->db->get('t_skp_bengkayang');
    }

    public function insert_skp_bengkayang($data)
    {
        $this->db->insert('t_skp_bengkayang',$data);
    }

    public function get_skp_belu()
    {
        return $this->db->get('t_skp_belu');
    }

    public function insert_skp_belu($data)
    {
        $this->db->insert('t_skp_belu',$data);
    }

    public function get_skp_sidenreng_rappang()
    {
        return $this->db->get('t_skp_sidenreng_rappang');
    }

    public function insert_skp_sidenreng_rappang($data)
    {
        $this->db->insert('t_skp_sidenreng_rappang',$data);
    }

    public function get_skp_bengkayang_by_skp($skp)
    {
        $this->db->where('skp',$skp);
        return $this->db->get('t_skp_bengkayang');
    }

    public function get_skp_belu_by_skp($skp)
    {
        $this->db->where('skp',$skp);
        return $this->db->get('t_skp_belu');
    }

    public function get_skp_sidrap_by_skp($skp)
    {
        $this->db->where('skp',$skp);
        return $this->db->get('t_skp_sidenreng_rappang');
    }
}
